<?php

namespace Rocket\Engine\Fuel;

use Rocket\Engine\Fuel\Contracts\Module;

class Engine
{
    protected $modules = [];

    public function addModule(Module $module)
    {
        $this->modules[] = $module;
    }

    public function routes()
    {
        foreach ($this->modules as $module) {
            $module->routes();
        }
    }
}
