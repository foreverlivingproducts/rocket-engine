<?php

namespace Rocket\Engine\Fuel;

use Illuminate\Support\ServiceProvider;

class FuelServiceProvider extends ServiceProvider
{
    /**
     * Define the resources used by Rocket.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Engine', Engine::class);
    }
}