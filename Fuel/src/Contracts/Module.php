<?php

namespace Rocket\Engine\Fuel\Contracts;

interface Module
{
    public function routes();
}
