# Manage your data-sets inside your Laravel app

The `rocket-engine/active-data` package provides an easy way to create active subsets of data.

Here's an example w/o using the subset for a simple relation:

```php
Posts::where('blog_id', $blog->id)->get();
```

And here's one using the subset.

```php
//if subset is based on blog_id
Posts::all();
```

The package allows the creation of global data, where the scope is null, this data is available to everyone and the behaviour of updating this data can be controlled, see `EditActiveDataContract`.

## Why do I need it?

It helps you avoid the hazzle of always checking permissions and sanity checking data.

We use this package to manage multi-site functionality in our e-commerce platform. It's an easy way to secure that data is not shared between sites.

We also use it in our CRM for the same reason, but instead of a multi-site installation, the dataset is based from the user itself.


## Installation

The package will automatically register itself. But you will have to setup what scope to use

```php
class MyServiceProvider 
{
    function boot()
    {
        ActiveData::SetScopeColumn('scope_id');
        ActiveData::SetScopeId(5);
    }
}
```

```php
class Posts extends Eloquent implements AddActiveDataContract, EditActiveDataContract
{
    use ActiveData;
}
```

`AddActiveDataContract` and `EditActiveDataContract` determines the behaviour when adding and editing the data subset of the model.

With `AddActiveDataContract` the scope is automatically set on the created entry.

With `EditActiveDataContract` **if** the scope was null it creates a new, updated, record but with the scope set. This is useful when you want default data that can be overriden by users and you don't want the original data changed.

## Defining the behaviour of `EditActiveDataContract`
```php
class Product extends Eloquent implements AddActiveDataContract, EditActiveDataContract
{
    use Activedata
    
    ...
    
    /**
     * Relations that you want to replicate along side the model.
     * If not set, it will point to the original relation
     *
     * @return array
     */
    public function replicateWithRelations()
    {
        return [
            'prices',
        ];
    }   
}
```

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.