<?php

namespace Rocket\Engine\ActiveData;

class ActiveData
{
    protected $scopeId = null;
    protected $scopeColumn = "scope_id";

    public function getScopeId()
    {
        return $this->scopeId;
    }

    public function getScopeColumn()
    {
        return $this->scopeColumn;
    }

    public function setScopeId($id)
    {
        $this->scopeId = $id;
    }

    public function setScopeColumn($column)
    {
        $this->scopeColumn = $column;
    }
}