<?php
namespace Rocket\Engine\ActiveData\Facades;

use Illuminate\Support\Facades\Facade;

class ActiveData extends Facade {
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ActiveData';
    }
}