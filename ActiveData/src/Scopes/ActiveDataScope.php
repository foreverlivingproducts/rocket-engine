<?php

namespace Rocket\Engine\ActiveData\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Support\Facades\DB;

class ActiveDataScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * This scope filters the data set of the giving model.
     * It filters out your site's items, but also includes default values if applicable.
     * Default values are represented as null values.
     * In order for default values to work properly you will need to have a common denominator for the 2 items, e.g a key.
     *
     * NOTE: The COALESCE is used to we can convert NULL to 0
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->whereIn($model->getTable() . '.id', function($query) use ($model)
        {
            $query->from($model->getTable())
                ->select($model->getTable() . '.id');

            if ($model->activeDataThroughRelation()) {
                $relation = $model->activeDataThroughRelation();
                $foreignColumn = isset($relation['foreign_column']) ? $relation['foreign_column'] : 'id';

                $query->join($relation['table'], function ($join) use ($relation, $model, $foreignColumn) {
                    $join->on($relation['table'] . '.' . $foreignColumn, '=', $model->getTable() . '.' . $relation['column']);
                });
            }

            $query->whereIn($model->getTableForActiveData() . '.id', function ($query) use ($model) {
                $query->from($model->getTableForActiveData())
                    ->select($model->getTableForActiveData() . '.id')
                    ->where('filter.' . $model->getGroupColumnForActiveData())
                    ->whereNested(function ($nested) use ($model) {
                        $nested->where($model->getQualifiedColumnForActiveData(), '=', $model->getScopeIdForActiveData())
                            ->orWhere($model->getQualifiedColumnForActiveData());
                    })
                    ->join($model->getTableForActiveData() . ' as filter', function ($join) use ($model) {
                        $join->on(DB::raw("COALESCE({$model->getQualifiedGroupColumnForActiveData()},0)"), '=', DB::raw("COALESCE(filter.{$model->getGroupColumnForActiveData()},0)"))
                            ->whereNested(function ($nested) use ($model) {
                                $nested->where("filter.{$model->getColumnForActiveData()}", '=', $model->getScopeIdForActiveData())
                                    ->orWhere("filter.{$model->getColumnForActiveData()}");
                            })
                            ->whereNested(function ($join2) use ($model) {
                                $join2->whereColumn(DB::raw("COALESCE({$model->getQualifiedGroupColumnForActiveData()},0)"), '<', DB::raw("COALESCE(filter.{$model->getGroupColumnForActiveData()},0)"))
                                    ->whereNested(function ($join3) use ($model) {
                                        $join3->on(DB::raw("COALESCE({$model->getQualifiedGroupColumnForActiveData()},0)"), '=', DB::raw("COALESCE(filter.{$model->getGroupColumnForActiveData()},0)"))
                                            ->whereColumn($model->getTableForActiveData() . '.id', '<', 'filter.id', 'AND');
                                    }, 'or');
                            });
                    }, null, null, 'left outer');
            });
        });
    }
}