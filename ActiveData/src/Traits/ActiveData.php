<?php

namespace Rocket\Engine\ActiveData\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\UnauthorizedException;
use Rocket\Engine\ActiveData\Scopes\ActiveDataScope;
use Rocket\Engine\ActiveData\Facades\ActiveData as ActiveDataFacade;

trait ActiveData {

    /**
     * Boot the scope.
     *
     * @return void
     */
    public static function bootActiveData()
    {
        static::addGlobalScope(new ActiveDataScope);
    }

    /**
     * Get the name of the column for applying the scope.
     *
     * @return string
     */
    public function getScopeIdForActiveData()
    {
        return ActiveDataFacade::getScopeId();
    }

    /**
     * Get the name of the column for applying the scope.
     *
     * @return string
     */
    public function getColumnForActiveData()
    {
        return ActiveDataFacade::getScopeColumn();
    }

    /**
     * Get the name of the column for applying the scope.
     *
     * @return string
     */
    public function getGroupColumnForActiveData()
    {
        return 'id';
    }

    /**
     * Get the name of the column for applying the scope.
     *
     * @return string
     */
    public function getTableForActiveData()
    {
        $relation = $this->activeDataThroughRelation();
        if ($relation) {
            return $relation['table'];
        }

        return $this->getTable();
    }

    /**
     * Get the fully qualified column name for applying the scope.
     *
     * @return string
     */
    public function getQualifiedColumnForActiveData()
    {
        return $this->getTableForActiveData() . '.' . $this->getColumnForActiveData();
    }

    /**
     * Get the fully qualified column name for applying the scope.
     *
     * @return string
     */
    public function getQualifiedGroupColumnForActiveData()
    {
        return $this->getTableForActiveData() . '.' . $this->getGroupColumnForActiveData();
    }

    /**
     * Get table relation if scope is derived
     *
     * @return bool|array
     */
    public function activeDataThroughRelation()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function getIsScopedAttribute()
    {
        if ($this->activeDataThroughRelation()) {
            return !is_null($this->{$this->activeDataThroughRelation()['attribute']}->{$this->getColumnForActiveData()});
        }

        return !is_null($this->{$this->getColumnForActiveData()});
    }

    /**
     * Get the query builder without the scope applied.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function withoutActiveDataScope()
    {
        return with(new static)->newQueryWithoutScope(new ActiveDataScope);
    }

    /**
     * Perform the actual delete query on this model instance.
     *
     * @return void
     */
    protected function performDeleteOnModel()
    {
        if (!$this->getIsScopedAttribute()) {
            throw new UnauthorizedException();
        }

        $relations = $this->activeDataThroughRelation();
        if (!$relations) {
            $this->setKeysForSaveQuery(
                $this->newQueryWithoutScopes())->where($this->getColumnForActiveData(),
                $this->{$this->getColumnForActiveData()}
            )->delete();

            $this->exists = false;
            return;
        }

        if ($this->{$relations['attribute']}->{$this->getColumnForActiveData()} == $this->getScopeIdForActiveData()) {
            return parent::performDeleteOnModel();
        }

        throw new UnauthorizedException();
    }

    protected function setKeysForSaveQuery(Builder $query)
    {
        $query = parent::setKeysForSaveQuery($query);

        if ($this->getIsScopedAttribute()) {
            $relations = $this->activeDataThroughRelation();
            if (!$relations) {
                $query->where(
                    $this->getQualifiedColumnForActiveData(),
                    '=',
                    $this->getAttribute($this->getColumnForActiveData())
                );
            }
        }

        return $query;
    }
}