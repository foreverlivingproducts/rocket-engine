<?php

namespace Rocket\Engine\ActiveData;

use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\ServiceProvider;
use Rocket\Engine\ActiveData\Contracts\AddActiveDataContract;
use Rocket\Engine\ActiveData\Contracts\EditActiveDataContract;
use Route;
use Event;
use ActiveData;

class ActiveDataServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(UrlGenerator $url)
    {
        Event::listen(['eloquent.creating: *'], function($event, $models) {
            if (!ActiveData::getScopeId()) {
                return;
            }

            foreach ($models as $model) {
                if (in_array(AddActiveDataContract::class,  class_implements($model))) {
                    if (!$model->is_scoped && !is_null($model->is_scoped)) {
                        if (!$model->activeDataThroughRelation()) {
                            $model->{ActiveData::getScopeColumn()} = ActiveData::getScopeId();
                        }
                    }
                }
            }
        });

        Event::listen('eloquent.updating: *', function($event, &$models) {
            if (!ActiveData::getScopeId()) {
                return;
            }

            foreach ($models as &$model) {
                if (in_array(EditActiveDataContract::class,  class_implements($model))) {
                    if (!$model->is_scoped && !is_null($model->is_scoped)) {
                        if (!$model->activeDataThroughRelation()) {
                            $model->{ActiveData::getScopeColumn()} = ActiveData::getScopeId();

                            $clone = $model->replicate();
                            $clone->save();

                            if (method_exists($model, 'replicateWithRelations')) {
                                $model->load(...$model->replicateWithRelations());

                                //re-sync everything
                                foreach ($model->getRelations() as $relationName => $items) {
                                    foreach ($items as $item) {
                                        $itemClone = $item->replicate();
                                        if (method_exists($itemClone, 'toFlatArray')) {
                                            $duplicatedData = $itemClone->toFlatArray();
                                        } else {
                                            $duplicatedData = $itemClone->toArray();
                                        }

                                        $clone->$relationName()->create($duplicatedData);
                                    }
                                }
                            }

                            $model->fill($clone->toArray());

                            return false;
                        }
                    }
                }
            }
        });
    }
    /**
     * Define the resources used by Rocket.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('ActiveData', 'Rocket\Engine\ActiveData\ActiveData');
    }
}