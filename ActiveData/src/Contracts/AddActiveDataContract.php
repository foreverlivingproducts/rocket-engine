<?php

namespace Rocket\Engine\ActiveData\Contracts;

/**
 * Use this contract to automatically set site_id depending
 * on what site you are currently visiting.
 *
 * Interface ActiveSiteModelContract
 * @package Rocket\Store
 */
interface AddActiveDataContract
{
}