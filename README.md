# Rocket Engine

## Installation

You can install the package via composer:

``` bash
...
"require": {
    "rocket/engine": "dev-master"
},
"repositories": {
    "rocket/engine": {
        "type": "vcs",
        "url": "https://bitbucket.org/foreverlivingproducts/rocket-engine.git"
    },
    "eighty8/laravel-seeder": {
        "type": "vcs",
        "url": "https://github.com/gluttons/laravel-seeder.git"
    }
}
...
```

The package will automatically register itself. But you will have to setup what scope to use


### Content
- [Fuel](Fuel/README.md)
- [Voyager](Voyager/README.md)
- [ActiveData](ActiveData/README.md)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.