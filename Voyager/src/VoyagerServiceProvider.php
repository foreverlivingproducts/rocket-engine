<?php

namespace Rocket\Engine\Voyager;

use Illuminate\Support\ServiceProvider;
use Engine;
use Voyager;

class VoyagerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Engine::addModule(new VoyagerModule());
        Voyager::useModel('DataType', DataType::class);
    }
}