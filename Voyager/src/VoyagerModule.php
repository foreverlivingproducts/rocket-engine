<?php

namespace Rocket\Engine\Voyager;

use Rocket\Engine\Fuel\Contracts\Module;
use Voyager;

class VoyagerModule implements Module
{
    public function routes()
    {
        Voyager::routes();
        require __DIR__.'/../routes/voyager.php';
    }
}