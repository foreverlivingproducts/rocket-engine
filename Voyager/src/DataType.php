<?php

namespace Rocket\Engine\Voyager;

use TCG\Voyager\Models\DataType as VoyagerDataType;

class DataType extends VoyagerDataType
{
    public function getRelationships($requestData, &$fields)
    {
        $requestData = parent::getRelationships($requestData, $fields);
        if (isset($requestData['relationships'])) {
            $relationships = $requestData['relationships'];
            if (count($relationships) > 0) {
                foreach ($relationships as $index => $relationship) {
                    $relationshipDetails = [];

                    if (isset($requestData['relationship_formfields_custom_'.$relationship])) {
                        $relationshipDetails['formfields_custom'] = $requestData['relationship_formfields_custom_'.$relationship];
                    }

                    $data = [];
                    if (isset($requestData['field_details_'.$relationship])) {
                        $data = json_decode($requestData['field_details_'.$relationship], true);
                    }

                    $requestData['field_details_'.$relationship] = json_encode(array_merge(
                        $data,
                        $relationshipDetails
                    ));
                }
            }
        }

        return $requestData;
    }

}
