<?php

namespace Rocket\Engine\Voyager\Http\Controllers;

use TCG\Voyager\Http\Controllers\VoyagerBaseController as Controller;
use Illuminate\Http\Request;
use Voyager;

class VoyagerBaseController extends Controller
{
    //***************************************
    //                ______
    //               |  ____|
    //               | |__
    //               |  __|
    //               | |____
    //               |______|
    //
    //  Edit an item of our Data Type BR(E)AD
    //
    //****************************************

    public function edit(Request $request, $content = null)
    {
        if ($content) {
            $id = (is_object($content) && isset($content->id) ? $content->id : $content);

            $slug = $this->getSlug($request);
            $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
            $relationships = $this->getRelationships($dataType);
            $dataTypeContent = (strlen($dataType->model_name) != 0)
                ? app($dataType->model_name)->with($relationships)->find($id)
                : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name
        }

        $unAuthorized = !$dataTypeContent->is_scoped && \Auth::user()->role->id != 1;
        if (is_null($content) || is_null($dataTypeContent) || $unAuthorized) {
            abort(401, "Unauthorized");
        }

        return parent::edit($request, $content);
    }
}
