<?php
namespace Rocket\Engine\Voyager;

use Eighty8\LaravelSeeder\Migration\MigratableSeeder;
use Illuminate\Support\Str;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Models\Role;
use Voyager;

abstract class BreadSeeder extends MigratableSeeder
{
    private $bread = [];

    protected function setupDataType($table, $name, $icon, $model, $controller = null, $policyName = null)
    {
        $this->bread = [
            'name'                  => $table,
            'slug'                  => $table,
            'table'                 => $table,
            'display_name_singular' => $name,
            'display_name_plural'   => Str::plural($name),
            'icon'                  => $icon,
            'model_name'            => $model,
            'generate_permissions'  => true,
            'server_side'           => true
        ];
        if ($controller) {
            $this->bread['controller'] = $controller;
        }
        if ($policyName) {
            $this->bread['policy_name'] = $policyName;
        }
    }

    protected function addDataRow($field, $details)
    {
        $data = [
            "field_order_{$field}" => 0,
            "field_{$field}" => $field
        ];
        $settings = explode('|', $details);

        foreach ($settings as $setting) {
            $values = explode(':', $setting);
            if (count($values) == 1) {
                $data["field_{$values[0]}_{$field}"] = true;
            } else {
                switch($values[0]) {
                    case "permissions":
                        $permissions = str_split($values[1]);
                        foreach($permissions as $permission){
                            switch ($permission) {
                                case 'b':
                                    $data["field_browse_{$field}"] = 1;
                                    break;
                                case 'r':
                                    $data["field_read_{$field}"] = 1;
                                    break;
                                case 'e':
                                    $data["field_edit_{$field}"] = 1;
                                    break;
                                case 'a':
                                    $data["field_add_{$field}"] = 1;
                                    break;
                                case 'd':
                                    $data["field_delete_{$field}"] = 1;
                                    break;
                            }
                        }
                        break;
                    case "type":
                        $data["field_input_type_{$field}"] = $values[1];
                        break;
                    case "options":
                        $options = [];
                        $optionValues = explode(',', $values[1]);
                        foreach ($optionValues as $optionValue) {
                            $options[$optionValue] = $optionValue;
                        }
                        $data["field_details_{$field}"] = json_encode(['options' => $options]);
                        break;
                    default:
                        $data["field_{$values[0]}_{$field}"] = $values[1];
                        break;
                }
            }
        }

        if (!isset($data["field_input_type_{$field}"])) {
            $data["field_input_type_{$field}"] = "text";
        }

        if (!isset($data["field_required_{$field}"])) {
            $data["field_required_{$field}"] = false;
        }

        if (!isset($data["field_details_{$field}"])) {
            $data["field_details_{$field}"] = "";
        }

        if (!isset($data["field_display_name_{$field}"])) {
            $data["field_display_name_{$field}"] = title_case(str_replace("_", " ", $field));
        }

        $this->bread = array_merge($this->bread, $data);
    }

    protected function addRelationship($related_table, $model, $details, $relation_type, $relation, $foreign_key = 'id', $related_key = 'id', $pivot_table = null, $taggable = null)
    {
        $data = [
            "relationship_model_{$relation}"                => $model,
            "relationship_table_{$relation}"                => $related_table,
            "relationship_type_{$relation}"                 => $relation_type,
            "relationship_column_{$relation}"               => $foreign_key,
            "relationship_key_{$relation}"                  => $foreign_key,
            "relationship_pivot_table_{$relation}"          => $pivot_table ?? $related_table,
            "relationship_pivot_{$relation}"                => $pivot_table ? 1 : 0,
            "relationship_column_belongs_to_{$relation}"    => $related_key,
            "relationship_label_{$relation}"                => $taggable ?? 'relation_label',
            "relationship_taggable_{$relation}"             => $taggable ? true : false,
        ];

        $settings = explode('|', $details);
        foreach ($settings as $setting) {
            $values = explode(':', $setting);
            if (count($values) == 1) {
                $data["relationship_{$values[0]}_{$relation}"] = true;
            } else {
                switch($values[0]) {
                    case "view":
                        $data["relationship_formfields_custom_{$relation}"] = $values[1];
                        break;
                    default:
                        break;
                }
            }
        }

        if (!isset($this->bread['relationships'])) {
            $this->bread['relationships'] = [];
        }
        $this->bread['relationships'][] = $relation;
        $this->bread = array_merge($this->bread, $data);

        $this->addDataRow($relation, $details . "|type:relationship");
    }

    protected function commit()
    {
        $dataType = Voyager::model('DataType');
        $dataType->updateDataType($this->bread, true);
    }

    protected function addMenuItem($order = 100, $parent = null)
    {
        $menu = Menu::where('name', '=', 'admin')->first();
        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => $this->bread['display_name_plural'],
            'url' => '',
            'icon_class' => $this->bread['icon'],
            'target' => '_self',
            'color' => null,
            'parent_id' => $parent,
            'order' => $order,
            'route' => "voyager.{$this->bread['slug']}.index"
        ]);
    }

    protected function addPermissions($roleName)
    {
        $role = Role::where('name', $roleName)->firstOrFail();
        $permissions = Permission::where('table_name', '=', $this->bread['table'])->get();

        $role->permissions()->attach(
            $permissions->pluck('id')->all()
        );
    }
}
