# Voyager


## Installation

The package will automatically register itself. But you will have to setup what scope to use

```php
class MyServiceProvider 
{
    function boot()
    {
        ActiveData::SetScopeColumn('scope_id');
        ActiveData::SetScopeId(5);
    }
}
```
