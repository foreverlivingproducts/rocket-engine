<?php
use TCG\Voyager\Events\Routing;
use TCG\Voyager\Events\RoutingAdmin;
use TCG\Voyager\Events\RoutingAdminAfter;
use TCG\Voyager\Events\RoutingAfter;
use TCG\Voyager\Models\DataType;

Route::group(['as' => 'voyager.'], function () {
    Route::group(['middleware' => 'admin.user'], function () {
        try {
            foreach (DataType::all() as $dataType) {
                $breadController = $dataType->controller
                    ? $dataType->controller
                    : '\\' . \Rocket\Engine\Voyager\Http\Controllers\VoyagerBaseController::class;

                Route::get($dataType->slug . '/order', $breadController . '@order')->name($dataType->slug . '.order');
                Route::post($dataType->slug . '/order', $breadController . '@update_order')->name($dataType->slug . '.order');
                Route::resource($dataType->slug, $breadController);
            }
        } catch (\InvalidArgumentException $e) {
            throw new \InvalidArgumentException("Custom routes hasn't been configured because: " . $e->getMessage(), 1);
        } catch (\Exception $e) {
            // do nothing, might just be because table not yet migrated.
        }
    });
});